package collector

type CollectorEntity struct {
	CPU CPUEntity `json:"cpu"`
	Mem MemEntity `json:"mem"`
}
type CPUEntity struct {
	Goroutines uint64 `json:"goroutines"`
	CgoCalls   uint64 `json:"cgo_calls"`
}
type HeapEntity struct {
	Alloc    uint64 `json:"alloc"`
	Sys      uint64 `json:"sys"`
	Idle     uint64 `json:"idle"`
	Inuse    uint64 `json:"inuse"`
	Released uint64 `json:"released"`
	Objects  uint64 `json:"objects"`
}
type StackEntity struct {
	Inuse       uint64 `json:"inuse"`
	Sys         uint64 `json:"sys"`
	MspanInuse  uint64 `json:"mspan_inuse"`
	MspanSys    uint64 `json:"mspan_sys"`
	McacheInuse uint64 `json:"mcache_inuse"`
	McacheSys   uint64 `json:"mcache_sys"`
}
type GcEntity struct {
	Sys        uint64 `json:"sys"`
	Next       uint64 `json:"next"`
	Last       uint64 `json:"last"`
	PauseTotal uint64 `json:"pause_total"`
	Pause      uint64 `json:"pause"`
	Count      uint64 `json:"count"`
	NumForced  uint64 `json:"num_forced"`
}
type MemEntity struct {
	Alloc    uint64      `json:"alloc"`
	Total    uint64      `json:"total"`
	Sys      uint64      `json:"sys"`
	Lookups  uint64      `json:"lookups"`
	Malloc   uint64      `json:"malloc"`
	Frees    uint64      `json:"frees"`
	Othersys uint64      `json:"othersys"`
	Heap     HeapEntity  `json:"heap"`
	Stack    StackEntity `json:"stack"`
	Gc       GcEntity    `json:"gc"`
}
