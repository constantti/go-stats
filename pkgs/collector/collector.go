package collector

import (
	"encoding/json"
	"fmt"
	"runtime"
	"time"
)

// GaugeFunc is an interface that implements the setting of a gauge value
// in a stats system. It should be expected that key will contain multiple
// parts separated by the '.' character in the form used by statsd (e.x.
// "mem.heap.alloc")
type GaugeFunc func(key string, val uint64)

// Collector implements the periodic grabbing of informational data from the
// runtime package and outputting the values to a GaugeFunc.
type Collector struct {
	// PauseDur represents the interval inbetween each set of stats output.
	// Defaults to 10 seconds.
	PauseDur time.Duration

	// EnableCPU determines whether CPU statisics will be output. Defaults to true.
	EnableCPU bool

	// EnableMem determines whether memory statistics will be output. Defaults to true.
	EnableMem bool

	// EnableGC determines whether garbage collection statistics will be output. EnableMem
	// must also be set to true for this to take affect. Defaults to true.
	EnableGC bool

	// Done, when closed, is used to signal Collector that is should stop collecting
	// statistics and the Run function should return. If Done is set, upon shutdown
	// all gauges will be sent a final zero value to reset their values to 0.
	Done <-chan struct{}

	gaugeFunc GaugeFunc
}

// New creates a new Collector that will periodically output statistics to gaugeFunc. It
// will also set the values of the exported fields to the described defaults. The values
// of the exported defaults can be changed at any point before Run is called.
func New(gaugeFunc GaugeFunc) *Collector {
	return &Collector{
		PauseDur:  10 * time.Second,
		EnableCPU: true,
		EnableMem: true,
		EnableGC:  true,
		gaugeFunc: gaugeFunc,
	}
}

// Run gathers statistics from package runtime and outputs them to the configured GaugeFunc every
// PauseDur. This function will not return until Done has been closed (or never if Done is nil),
// therefore it should be called in its own goroutine.
func (c *Collector) Run() {
	defer c.zeroStats()
	c.outputStats()

	// Gauges are a 'snapshot' rather than a histogram. Pausing for some interval
	// aims to get a 'recent' snapshot out before statsd flushes metrics.
	tick := time.NewTicker(c.PauseDur)
	defer tick.Stop()
	for {
		select {
		case <-c.Done:
			return
		case <-tick.C:
			c.outputStats()
			c.zeroStats()
		}
	}
}

// zeroStats sets all the stat guages to zero. On shutdown we want to zero them out so they don't persist
// at their last value until we start back up.
func (c *Collector) zeroStats() {
	output := CollectorEntity{}
	if c.EnableCPU {
		c.outputCPUStats(&output)
	}
	if c.EnableMem {
		mStats := runtime.MemStats{}
		c.outputMemStats(&mStats, &output)
		if c.EnableGC {
			c.outputGCStats(&mStats, &output)
		}
	}
}

func (c *Collector) outputStats() {

	output := CollectorEntity{}

	if c.EnableCPU {
		c.outputCPUStats(&output)
	}
	if c.EnableMem {
		m := &runtime.MemStats{}
		runtime.ReadMemStats(m)
		c.outputMemStats(m, &output)
		if c.EnableGC {
			c.outputGCStats(m, &output)
		}
	}

	outputString, err := json.Marshal(output)
	if err != nil {
		fmt.Printf("Error: %s", err)
		return
	}

	c.gaugeFunc(string(outputString), 1)
}

func (c *Collector) outputCPUStats(ce *CollectorEntity) {

	ce.CPU.Goroutines = uint64(runtime.NumGoroutine())
	ce.CPU.CgoCalls = uint64(runtime.NumCgoCall())
}

func (c *Collector) outputMemStats(m *runtime.MemStats, ce *CollectorEntity) {
	// General
	ce.Mem.Alloc = m.Alloc
	ce.Mem.Total = m.TotalAlloc
	ce.Mem.Sys = m.Sys
	ce.Mem.Lookups = m.Lookups
	ce.Mem.Malloc = m.Mallocs
	ce.Mem.Frees = m.Frees
	ce.Mem.Othersys = m.OtherSys

	// Heap
	ce.Mem.Heap.Alloc = m.HeapAlloc
	ce.Mem.Heap.Sys = m.HeapSys
	ce.Mem.Heap.Idle = m.HeapIdle
	ce.Mem.Heap.Inuse = m.HeapInuse
	ce.Mem.Heap.Released = m.HeapReleased
	ce.Mem.Heap.Objects = m.HeapObjects

	// Stack
	ce.Mem.Stack.Inuse = m.StackInuse
	ce.Mem.Stack.Sys = m.StackSys
	ce.Mem.Stack.MspanInuse = m.MSpanInuse
	ce.Mem.Stack.MspanSys = m.MSpanSys
	ce.Mem.Stack.McacheInuse = m.MCacheInuse
	ce.Mem.Stack.McacheSys = m.MCacheSys

}

func (c *Collector) outputGCStats(m *runtime.MemStats, ce *CollectorEntity) {
	ce.Mem.Gc.Sys = m.GCSys
	ce.Mem.Gc.Next = m.NextGC
	ce.Mem.Gc.Last = m.LastGC
	ce.Mem.Gc.PauseTotal = m.PauseTotalNs
	ce.Mem.Gc.Pause = m.PauseNs[(m.NumGC+255)%256]
	ce.Mem.Gc.Count = uint64(m.NumGC)
	ce.Mem.Gc.NumForced = uint64(m.NumForcedGC)
}
