package runstats

import (
	"flag"
	"fmt"
	"os"
	"runtime"
	"strconv"
	"time"

	"gitlab.com/constantti/go-stats/pkgs/collector"
	"gitlab.com/constantti/go-stats/pkgs/ip"
	"gitlab.com/constantti/send2statsd"
)

var (
	statsd     *string = flag.String("statsd", "127.0.0.1:1053", "Statsd host:port pair")
	prefix     *string = flag.String("metric-prefix", "<detect-hostname>", "Metric prefix path; detects the local hostname by default")
	prefixSend *string = flag.String("send-prefix", "sender", "Prefix to send information")
	pause      *int    = flag.Int("pause", 10, "Collection pause interval")

	publish *bool = flag.Bool("publish-runtime-stats", true, "Collect go runtime statistics")
	cpu     *bool = flag.Bool("cpu", true, "Collect CPU Statistics")
	mem     *bool = flag.Bool("mem", true, "Collect Memory Statistics")
	gc      *bool = flag.Bool("gc", true, "Collect GC Statistics (requires Memory be enabled)")
)

func init() {
	go runCollector()
}

func runCollector() {
	for !flag.Parsed() {
		// Defer execution of this goroutine.
		runtime.Gosched()

		// Add an initial delay while the program initializes to avoid attempting to collect
		// metrics prior to our flags being available / parsed.
		time.Sleep(1 * time.Second)
	}

	s, err := send2statsd.Dial("udp", *statsd)
	if err != nil {
		panic(fmt.Sprintf("Unable to connect to Statsd on %s - %s", *statsd, err))
	}

	currentTime := string(time.Now().Format(time.RFC3339))
	localIp, err := ip.ExternalIP()
	if err != nil {
		fmt.Println(err)
	}
	*prefixSend = currentTime + "|"
	*prefixSend += localIp + "|"

	if os.Getenv("TRIBE") != "" {
		*prefixSend += os.Getenv("TRIBE") + "|"
	}
	if os.Getenv("SQUAD") != "" {
		*prefixSend += os.Getenv("SQUAD") + "|"
	}
	if os.Getenv("APP") != "" {
		*prefixSend += os.Getenv("APP") + "|"
	}

	if *prefix == "<detect-hostname>" {
		hn, err := os.Hostname()
		if err != nil {
			*prefixSend += "unknown"
		} else {
			*prefixSend += hn
		}
	}
	*prefixSend += "|"

	gaugeFunc := func(key string, val uint64) {
		s.Gauge(1.0, *prefixSend+key, strconv.FormatUint(val, 10))
	}

	c := collector.New(gaugeFunc)
	c.PauseDur = time.Duration(*pause) * time.Second
	c.EnableCPU = *cpu
	c.EnableMem = *mem
	c.EnableGC = *gc

	if *publish {
		c.Run()
	}
}
